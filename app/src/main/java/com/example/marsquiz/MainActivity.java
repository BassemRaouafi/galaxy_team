package com.example.marsquiz;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ActionBar;
import android.app.Dialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    Button btn_start;
    Dialog dialog;
    SharedPreferences preferences;
    int coins =50;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_main);
        preferences = getApplicationContext().getSharedPreferences("MyPreferences", getApplication().MODE_PRIVATE);
        btn_start = findViewById(R.id.btn_start);



        btn_start.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(preferences.getInt("money", 0)==0){

                    ShowDialog();

                }else {
                    startActivity(new Intent(getApplicationContext(), HazardsActivity.class));
                }


            }
        });

    }

    private void ShowDialog() {
        Button getmoney;
        TextView tv_coins;

        dialog = new Dialog(MainActivity.this);
        dialog.setContentView(R.layout.money_dialog);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        Window window = dialog.getWindow();
        window.setGravity(Gravity.CENTER);
        window.getAttributes().windowAnimations = R.style.DialogAnimation;
        getmoney = dialog.findViewById(R.id.btn_next);
        tv_coins = dialog.findViewById(R.id.tv_coinss);
        tv_coins.setText(String.valueOf(coins));
        getmoney.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences.Editor editor = preferences.edit();
                editor.putInt("money", coins);
                editor.commit();
                dialog.dismiss();
                startActivity(new Intent(getApplicationContext(), HazardsActivity.class));

            }
        });
        dialog.setCancelable(true);
        window.setLayout(ActionBar.LayoutParams.WRAP_CONTENT,ActionBar.LayoutParams.WRAP_CONTENT);
        dialog.show();

    }


}