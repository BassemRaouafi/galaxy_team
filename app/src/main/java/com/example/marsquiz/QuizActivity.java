package com.example.marsquiz;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;

import android.app.ActionBar;
import android.app.Dialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

public class QuizActivity extends AppCompatActivity {

    private TextView questions;
    private TextView question, tt;
    private ImageView backBtn;
    private AppCompatButton option1, option2, option3, option4;
    private AppCompatButton nextBtn;
    private List<QuestionsList> questionsLists;
    private int currentQuestionPosition = 0;
    private String selectedOptionByUser = "";
    Dialog dialog;
    double min;
    SharedPreferences preferences;
    int m;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_quiz);
        backBtn = findViewById(R.id.imgv_back_btn);
        //final TextView timer = findViewById(R.id.timer);

        dialog = new Dialog(this);
        preferences = getApplicationContext().getSharedPreferences("MyPreferences", getApplication().MODE_PRIVATE);
        questions = findViewById(R.id.questions);
        question = findViewById(R.id.question);
        option1 = findViewById(R.id.option1);
        option2 = findViewById(R.id.option2);
        option3 = findViewById(R.id.option3);
        option4 = findViewById(R.id.option4);
        nextBtn = findViewById(R.id.nextBtn);
        tt = findViewById(R.id.timer);


        m = preferences.getInt("money", 0);
        tt.setText(String.valueOf(m));
        min = getIntent().getIntExtra("min", 2);

        questionsLists = DataQuestions.getQuestions(getIntent().getIntExtra("numHazard", 0));

        questions.setText((currentQuestionPosition + 1) + "/" + questionsLists.size());
        question.setText(questionsLists.get(0).getQuestion());

        verifOption(currentQuestionPosition);


        option1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (selectedOptionByUser.isEmpty()) {
                    selectedOptionByUser = option1.getText().toString();
                    option1.setBackgroundResource(R.drawable.round_back_red10);
                    option1.setTextColor(Color.WHITE);
                    revealAnswer();
                    questionsLists.get(currentQuestionPosition).setUserSelectedAnswer(selectedOptionByUser);

                }
            }
        });

        option2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (selectedOptionByUser.isEmpty()) {
                    selectedOptionByUser = option2.getText().toString();
                    option2.setBackgroundResource(R.drawable.round_back_red10);
                    option2.setTextColor(Color.WHITE);
                    revealAnswer();
                    questionsLists.get(currentQuestionPosition).setUserSelectedAnswer(selectedOptionByUser);

                }
            }
        });

        option3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (selectedOptionByUser.isEmpty()) {
                    selectedOptionByUser = option3.getText().toString();
                    option3.setBackgroundResource(R.drawable.round_back_red10);
                    option3.setTextColor(Color.WHITE);
                    revealAnswer();
                    questionsLists.get(currentQuestionPosition).setUserSelectedAnswer(selectedOptionByUser);

                }
            }
        });
        option4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (selectedOptionByUser.isEmpty()) {
                    selectedOptionByUser = option4.getText().toString();
                    option4.setBackgroundResource(R.drawable.round_back_red10);
                    option4.setTextColor(Color.WHITE);
                    revealAnswer();
                    questionsLists.get(currentQuestionPosition).setUserSelectedAnswer(selectedOptionByUser);

                }
            }
        });

        nextBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (selectedOptionByUser.isEmpty()) {
                    Toast.makeText(QuizActivity.this, "Please select an option", Toast.LENGTH_SHORT).show();
                } else {
                    changeNextQuestion();
                }

            }
        });
        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), HazardDetailsActivity.class);
                intent.putExtra("numHazard", getIntent().getIntExtra("numHazard", 0));
                intent.putExtra("min", min);
                startActivity(intent);
                finish();

            }
        });

    }


    private void changeNextQuestion() {
        currentQuestionPosition++;
        if ((currentQuestionPosition + 1) == questionsLists.size()) {
            nextBtn.setText("Submit Quiz");
        }
        if (currentQuestionPosition < questionsLists.size()) {
            selectedOptionByUser = "";

            option1.setBackgroundResource(R.drawable.round_back_white_stroke2_10);
            option1.setTextColor(Color.parseColor("#1F6BB8"));

            option2.setBackgroundResource(R.drawable.round_back_white_stroke2_10);
            option2.setTextColor(Color.parseColor("#1F6BB8"));

            option3.setBackgroundResource(R.drawable.round_back_white_stroke2_10);
            option3.setTextColor(Color.parseColor("#1F6BB8"));

            option4.setBackgroundResource(R.drawable.round_back_white_stroke2_10);
            option4.setTextColor(Color.parseColor("#1F6BB8"));


            questions.setText((currentQuestionPosition + 1) + "/" + questionsLists.size());
            question.setText(questionsLists.get(currentQuestionPosition).getQuestion());

            verifOption(currentQuestionPosition);
        } else {
            showPopup();
        }
    }

    private void showPopup() {

        if (getCorrectAnswers() >= min) {
            ShowWinDialog();
        } else {
            ShowLoseDialog();
        }
    }


    private int getCorrectAnswers() {

        int correctAnswers = 0;
        for (int i = 0; i < questionsLists.size(); i++) {
            final String getUserSelectedAnswer = questionsLists.get(i).getUserSelectedAnswer();
            final String getAnswer = questionsLists.get(i).getAnswer();
            if (getUserSelectedAnswer.equals(getAnswer)) {
                correctAnswers++;
            }

        }
        return correctAnswers;
    }


    private int getInCorrectAnswers() {

        int inCorrectAnswers = 0;
        for (int i = 0; i < questionsLists.size(); i++) {
            final String getUserSelectedAnswer = questionsLists.get(i).getUserSelectedAnswer();
            final String getAnswer = questionsLists.get(i).getAnswer();
            if (!getUserSelectedAnswer.equals(getAnswer)) {
                inCorrectAnswers++;
            }

        }
        return inCorrectAnswers;
    }

    @Override
    public void onBackPressed() {
       /*quizTimer.purge();
       quizTimer.cancel();*/
        startActivity(new Intent(getApplicationContext(), MainActivity.class));
        finish();
    }

    private void revealAnswer() {
        final String getAnswer = questionsLists.get(currentQuestionPosition).getAnswer();
        if (option1.getText().toString().equals(getAnswer)) {
            option1.setBackgroundResource(R.drawable.round_back_green);
            option1.setTextColor(Color.WHITE);
        } else if (option2.getText().toString().equals(getAnswer)) {
            option2.setBackgroundResource(R.drawable.round_back_green);
            option2.setTextColor(Color.WHITE);
        } else if (option3.getText().toString().equals(getAnswer)) {
            option3.setBackgroundResource(R.drawable.round_back_green);
            option3.setTextColor(Color.WHITE);
        } else if (option4.getText().toString().equals(getAnswer)) {
            option4.setBackgroundResource(R.drawable.round_back_green);
            option4.setTextColor(Color.WHITE);
        }
    }


    private void ShowWinDialog() {
        Button btn_next;
        TextView tv_money;
        dialog = new Dialog(QuizActivity.this);
        dialog.setContentView(R.layout.win_dialog);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        Window window = dialog.getWindow();
        window.setGravity(Gravity.CENTER);
        window.getAttributes().windowAnimations = R.style.DialogAnimation;
        btn_next = dialog.findViewById(R.id.btn_next);
        tv_money = dialog.findViewById(R.id.tv_coinss);
        btn_next.setBackgroundResource(R.drawable.green);

        tv_money.setText(String.valueOf(getCorrectAnswers()));
        btn_next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();

                SharedPreferences.Editor editor = preferences.edit();
                editor.putInt("step", getIntent().getIntExtra("numHazard", 1) + 1);
                int sc = preferences.getInt("money", 0);
                sc = sc + getCorrectAnswers();
                editor.putInt("money", sc);
                editor.commit();

                if (preferences.getInt("step", 0) >= 6) {
                    editor.putInt("step", 1);
                    editor.putInt("money", 50);
                    startActivity(new Intent(getApplicationContext(), EndOfGameActivity.class));
                    finish();
                } else {
                    startActivity(new Intent(getApplicationContext(), HazardsActivity.class));
                    finish();
                }
            }
        });
        dialog.setCancelable(true);
        window.setLayout(ActionBar.LayoutParams.WRAP_CONTENT, ActionBar.LayoutParams.WRAP_CONTENT);
        dialog.show();
    }

    private void ShowLoseDialog() {
        Button btn_try_again;
        dialog = new Dialog(QuizActivity.this);
        dialog.setContentView(R.layout.lose_dialog);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        Window window = dialog.getWindow();
        window.setGravity(Gravity.CENTER);
        window.getAttributes().windowAnimations = R.style.DialogAnimation;
        btn_try_again = dialog.findViewById(R.id.btn_try_again);
        btn_try_again.setBackgroundResource(R.drawable.green);

        btn_try_again.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.dismiss();
                recreate();


            }
        });
        dialog.setCancelable(true);
        window.setLayout(ActionBar.LayoutParams.WRAP_CONTENT, ActionBar.LayoutParams.WRAP_CONTENT);
        dialog.show();

    }


    private void verifOption(int i) {


        if (questionsLists.get(i).getOption1() == null) {
            option1.setVisibility(View.INVISIBLE);

        } else {
            option1.setVisibility(View.VISIBLE);
            option1.setText(questionsLists.get(i).getOption1());

        }

        if (questionsLists.get(i).getOption2() == null) {
            option2.setVisibility(View.INVISIBLE);

        } else {
            option2.setVisibility(View.VISIBLE);
            option2.setText(questionsLists.get(i).getOption2());

        }

        if (questionsLists.get(i).getOption3() == null) {
            option3.setVisibility(View.INVISIBLE);

        } else {
            option3.setVisibility(View.VISIBLE);
            option3.setText(questionsLists.get(i).getOption3());


        }

        if (questionsLists.get(i).getOption4() == null) {
            option4.setVisibility(View.INVISIBLE);


        } else {
            option4.setVisibility(View.VISIBLE);
            option4.setText(questionsLists.get(i).getOption4());

        }

    }

}