package com.example.marsquiz;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import android.app.ActionBar;
import android.app.Dialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class HazardsActivity extends AppCompatActivity {

    int step=0;
    int myvar = -1;
    CardView hazard1,hazard2,hazard3,hazard4,hazard5;

    RelativeLayout lt_hazard1,lt_hazard2,lt_hazard3,lt_hazard4,lt_hazard5;
    TextView money;
    Dialog dialog;
    ImageView closePopupPositiveImg;
    SharedPreferences preferences;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_hazards);
         preferences = getApplicationContext().getSharedPreferences("MyPreferences", getApplication().MODE_PRIVATE);
         myvar = preferences.getInt("money", 0);
        dialog = new Dialog(this);
        init();
        if (myvar != -1)
        {
            money.setText(String.valueOf(myvar));
        }
        if(preferences.getInt("step", 0)>step) {
            step = preferences.getInt("step", 0);

        }



        lt_hazard2= findViewById(R.id.lt_hazard2);
        lt_hazard3= findViewById(R.id.lt_hazard3);
        lt_hazard4= findViewById(R.id.lt_hazard4);
        lt_hazard5= findViewById(R.id.lt_hazard5);




        changestep(step);



        hazard1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(getApplicationContext(), HazardDetailsActivity.class);
                intent.putExtra("numHazard", 1);
                intent.putExtra("min",2);
                startActivity(intent);
                finish();
            }
        });

        hazard2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(step<2) {
                    showpopup();
                }else{
                    Intent intent = new Intent(getApplicationContext(), HazardDetailsActivity.class);
                    intent.putExtra("numHazard", 2);
                    intent.putExtra("min",1);
                    startActivity(intent);
                    finish();
                }
                //step++;


            }
        });


        hazard3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(step<3) {
                    showpopup();
                }else {
                    Intent intent = new Intent(getApplicationContext(), HazardDetailsActivity.class);
                    intent.putExtra("numHazard", 3);
                    intent.putExtra("min", 2);

                    startActivity(intent);
                    finish();
                }
            }
        });

        hazard4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
 if(step<4) {
                    showpopup();
                }else {
     Intent intent = new Intent(getApplicationContext(), HazardDetailsActivity.class);
     intent.putExtra("numHazard", 4);
     intent.putExtra("min", 3);
     startActivity(intent);
     finish();
 }

            }
        });

        hazard5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                 if(step<5) {
                    showpopup();
                }else{
                     Intent intent = new Intent(getApplicationContext(), HazardDetailsActivity.class);
                     intent.putExtra("numHazard", 5);
                     intent.putExtra("min",2);
                     startActivity(intent);
                     finish();
                 }

            }
        });



    }

    private void changestep(int step) {
        switch (step) {
            case 1:
                lt_hazard2.setBackgroundResource(R.color.defaultcolor);
                lt_hazard3.setBackgroundResource(R.color.defaultcolor);
                lt_hazard4.setBackgroundResource(R.color.defaultcolor);
                lt_hazard5.setBackgroundResource(R.color.defaultcolor);
                break;
            case 2 :
                lt_hazard2.setBackgroundResource(R.color.light_pink);
                lt_hazard3.setBackgroundResource(R.color.defaultcolor);
                lt_hazard4.setBackgroundResource(R.color.defaultcolor);
                lt_hazard5.setBackgroundResource(R.color.defaultcolor);
                break;
            case 3:
                lt_hazard2.setBackgroundResource(R.color.light_pink);
                lt_hazard3.setBackgroundResource(R.color.purple);
                lt_hazard4.setBackgroundResource(R.color.defaultcolor);
                lt_hazard5.setBackgroundResource(R.color.defaultcolor);
                break;
            case 4:
                lt_hazard2.setBackgroundResource(R.color.light_pink);
                lt_hazard3.setBackgroundResource(R.color.purple);
                lt_hazard4.setBackgroundResource(R.color.lime);
                lt_hazard5.setBackgroundResource(R.color.defaultcolor);
                break;
            case 5:
                lt_hazard2.setBackgroundResource(R.color.light_pink);
                lt_hazard3.setBackgroundResource(R.color.purple);
                lt_hazard4.setBackgroundResource(R.color.lime);
                lt_hazard5.setBackgroundResource(R.color.light_orange);
                break;


        }
    }

    private void init(){
        hazard1 = findViewById(R.id.hazard1);
        hazard2 = findViewById(R.id.hazard2);
        hazard3 = findViewById(R.id.hazard3);
        hazard4 = findViewById(R.id.hazard4);
        hazard5 = findViewById(R.id.hazard5);
        money = findViewById(R.id.money);
    }


   private void showpopup() {

       dialog = new Dialog(HazardsActivity.this);
       dialog.setContentView(R.layout.locked_dialog);
       dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
       Window window = dialog.getWindow();
       window.setGravity(Gravity.CENTER);
       window.getAttributes().windowAnimations = R.style.DialogAnimation;
       dialog.setCancelable(true);
       window.setLayout(ActionBar.LayoutParams.WRAP_CONTENT,ActionBar.LayoutParams.WRAP_CONTENT);
       dialog.show();

        /*dialog.setContentView(R.layout.popup_error);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        closePopupPositiveImg = dialog.findViewById(R.id.closePopupPositiveImg);

        closePopupPositiveImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.show();*/
    }


}