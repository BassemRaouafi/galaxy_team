package com.example.marsquiz;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

public class HazardDetailsActivity extends AppCompatActivity {

     Button btn_startQuizz;
     ImageView back_btn;
    TextView hazardDetails,hazardTitle;
int numHazard;
String details="";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_hazard_details);
         hazardDetails= findViewById(R.id.hazardDetails);
         hazardTitle= findViewById(R.id.title_hazard);
         btn_startQuizz =  findViewById(R.id. btn_startQuizz);
         back_btn =  findViewById(R.id. back_btn);
        numHazard = getIntent().getIntExtra("numHazard",1);
        switch (numHazard) {
            case  1:
                hazardDetails.setText(R.string.hazard1_details);
                hazardTitle.setText(R.string.hazard1_title);

                break;
            case 2:
                hazardDetails.setText(R.string.hazard2_details);
                hazardTitle.setText(R.string.hazard2_title);
                break;
            case 3:
                hazardDetails.setText(R.string.hazard3_details);
                hazardTitle.setText(R.string.hazard3_title);
                break;
            case 4:
                hazardDetails.setText(R.string.hazard4_details);
                hazardTitle.setText(R.string.hazard4_title);
                break;
            case 5:
                hazardDetails.setText(R.string.hazard5_details);
                hazardTitle.setText(R.string.hazard5_title);
                break;
            default:
                hazardDetails.setText(R.string.hazard1_details);
                hazardTitle.setText(R.string.hazard1_title);
                break;
        }







        btn_startQuizz.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent =new Intent(getApplicationContext(), QuizActivity.class);
                intent.putExtra("numHazard", numHazard);
                intent.putExtra("min",getIntent().getIntExtra("min",2));
                startActivity(intent);
                finish();

            }
        });


        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(), HazardsActivity.class));
                finish();
            }
        });




    }
}