package com.example.marsquiz;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.WindowManager;

import com.hanks.htextview.base.HTextView;

public class EndOfGameActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_end_of_game);
        final HTextView textView1 = (HTextView) findViewById(R.id.textView6);
        textView1.setAnimationListener(new Animation(this));
        textView1.animateText(getString(R.string.endmessage));

    }
}