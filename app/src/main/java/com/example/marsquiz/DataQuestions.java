package com.example.marsquiz;

import java.util.ArrayList;
import java.util.List;

public class DataQuestions {
    private  static List<QuestionsList>  lesson_1_Questions() {
        final List<QuestionsList> questionsLists = new ArrayList<>();

        final  QuestionsList question1= new QuestionsList("choose the correct answer :", "Radiation on Mars are the same on Earth.", "Radiation on Mars are more dangerous than Earth.", "Radiation on Mars are less dangerous than Earth.", null, "Radiation on Mars are more dangerous than Earth.", "");
        final  QuestionsList question2= new QuestionsList("Radiation exposure may increase your Canser risk ? ", "True", "False", null, null, "True", "");
        final  QuestionsList question3= new QuestionsList("Radiation Exposure doesn't damage your central nervous system ? ", "True", "False", null, null, "False", "");
        final  QuestionsList question4= new QuestionsList("the International Space Station sits just within Earth's protective magnetic field, astronauts are still exposed to : ", "20 times higher radiation than on Earth.", "10 Times higher radiation than on Earth.", "4 Times higher radiation than on Earth.", "The same amout of radiation on Earth.", "10 Times higher radiation than on Earth.", "");

        questionsLists.add(question1);
        questionsLists.add(question2);
        questionsLists.add(question3);
        questionsLists.add(question4);

        return  questionsLists;
    }

    private  static List<QuestionsList>  lesson_2_Questions() {
        final List<QuestionsList> questionsLists = new ArrayList<>();

        final  QuestionsList question1= new QuestionsList("The selection of the crews on the Mars mission is : ", "The same as the Astronauts space station.", "More scrutiny and prepared.", null, null, "More scrutiny and prepared.", "");
        final  QuestionsList question2= new QuestionsList("How many extra minutes of daylight are on Mars ? ", "18", "28", "38", "42", "38", "");

        questionsLists.add(question1);
        questionsLists.add(question2);

        return  questionsLists;
    }

    private  static List<QuestionsList>  lesson_3_Questions() {
        final List<QuestionsList> questionsLists = new ArrayList<>();

        final  QuestionsList question1= new QuestionsList("Distance from Earth to moon are :", "150,120 miles.", "238,855 miles.", "354,487 miles.", "47,841 miles.", "238,855 miles.", "");
        final  QuestionsList question2= new QuestionsList("Distance from Earth to Mars are : ", "40 million miles.", "90 million miles.", "120 million miles.", "140 million miles.", "140 million miles.", "");
        final  QuestionsList question3= new QuestionsList("The communication delay from Mars up to : ", "4 minutes one-way.", "12 minutes one-way.", "20 minutes one-way.", "32 minutes one-way.", "20 minutes one-way.", "");
        final  QuestionsList question4= new QuestionsList("How long does it take to get to Mars? ", "around six months.", "a year.", "a week.", null, "around six months.", "");

        questionsLists.add(question1);
        questionsLists.add(question2);
        questionsLists.add(question3);
        questionsLists.add(question4);

        return  questionsLists;
    }


    private  static List<QuestionsList>  lesson_4_Questions() {
        final List<QuestionsList> questionsLists = new ArrayList<>();

        final  QuestionsList question1= new QuestionsList("How many gravity fields the astronauts will encounter on the journey to Mars ? ", "4", "5", "3", "1", "3", "");
        final  QuestionsList question2= new QuestionsList("On the journey through deep space, the astronaut will experience weightlessness ? ", "True", "False", null, null, "True", "");
        final  QuestionsList question3= new QuestionsList("Gravity on Mars are :", "3/6 of Earth's gravity.", "3/8 of Earth's gravity.", "Equal to Earth's gravity.", "1/12 of Earth's gravity.", "3/8 of Earth's gravity.", "");
        final  QuestionsList question4= new QuestionsList("Without Gravity working on your body, your bone density:", "Increases by 12% per month.", "Increases by 1% per month.", "Decreases by 12% per month.", "Decreases by 1% per month.", "Decreases by 1% per month.", "");
        final  QuestionsList question5= new QuestionsList("Transitioning from one gravity field to another affects your Heart ?", "True", "False", null, null, "True", "");
        questionsLists.add(question1);
        questionsLists.add(question2);
        questionsLists.add(question3);
        questionsLists.add(question4);
        questionsLists.add(question5);

        return  questionsLists;
    }

    private  static List<QuestionsList>  lesson_5_Questions() {
        final List<QuestionsList> questionsLists = new ArrayList<>();

        final  QuestionsList question1= new QuestionsList("The space environment is hostile and unsuitable for humans ?", "True", "False", null, null, "True", "");
        final  QuestionsList question2= new QuestionsList("Microbes can't change characteristics in space ? ", "True", "False", null, null, "False", "");
        final  QuestionsList question3= new QuestionsList("Astronaut's urine and blood samples are analyzed to monitor health and reveal important data? ", "True", "False", null, null, "True", "");

        questionsLists.add(question1);
        questionsLists.add(question2);
        questionsLists.add(question3);


        return  questionsLists;
    }

    public static List<QuestionsList> getQuestions(int NumLesson){
        switch (NumLesson) {
            case 1:
                return  lesson_1_Questions();
            case 2:
                return lesson_2_Questions();
            case 3:
                return lesson_3_Questions();
            case 4:
                return lesson_4_Questions();
            case 5:
                return lesson_5_Questions();
            default:
                return lesson_1_Questions();
        }
    }
}
